const array1 = [1,2,3];
const a = 'apple';

const foo = (arr, number) => {
  a = 'orange';
  arr.push(4);

  if(number) {
    number = Math.random();
  }

  console.log(arr, number) // ?

  return { arr, number }
}

const { arr: array2, number} = foo(array1, 1);

console.log('array1: ', array1); // ?
console.log('array2: ', array2);  // ?

console.log('number: ', number); // ?

console.log(array1 === array2); // ?