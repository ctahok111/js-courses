const obj = {};

function foo() {}
const bar = () => {}

foo.x = obj;
bar.x = obj;

console.log(foo.x == bar.x) // ?
console.log(foo.x === bar.x) // ?