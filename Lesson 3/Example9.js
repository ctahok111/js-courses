const foo = () => {
    let count = 0;
    return function() {
      count++;
      return count
    }
  }
  
  let a = foo()
  
  console.log(a()); // ?
  console.log(a()); // ?
  console.log(a()); // ?