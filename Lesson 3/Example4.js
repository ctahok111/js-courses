const x = 10;

function foo() {
    const x = 20;
    console.log(x); // ?
}

(function(someFunc) {
    const x = 30;

    someFunc()
})(foo)

