Promise.resolve(10)
  .then(e => console.log('1: ', e)) // ?
  .then(e => Promise.resolve(e))
  .then(e => {
    if (!e) {
      throw 'Error caught';
    }
  })
  .catch(e => {
    console.log('2: ',e); // ?
    return new Error('New error');
  })
  .then(e => {
    console.log('3 ', e.message); // ?
  })
  .catch(e => {
    console.log('4 ', e.message); // ?
  })
  .finally(e => {
      console.log('5 ', e) // ?
  });