function foo() {}

foo.food = 'pizza'
foo.name = 'aston'
foo.getFullName = (name, surname) => `${name} ${surname}`; 

console.log(foo.food) // ?
console.log(foo.name) // ?
console.log(foo.getFullName('Кекс', "Кунжутович")) // ?