const arr = ['firstValue', 2];

const push = arr.push('hello');
const pop = arr.pop();
const unshift = arr.unshift('show me your code');
const shift = arr.shift('');

console.log("Знаечние переменной push: ", push); // [...]
console.log("Знаечние переменной pop: ", pop); // hello
console.log("Знаечние переменной unshift: ", unshift);
console.log("Знаечние переменной shift: ", shift);
console.log("Знаечние переменной arr: ", arr);


// Мутирующие: 
// copyWithin()
// fill()
// pop()
// push()
// reverse()
// shift()
// sort()
// splice()