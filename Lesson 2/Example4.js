const obj = { a: 1 };

console.log(Object.is(obj, obj)); // ?
console.log(Object.is([], [])); // ?  
console.log(Object.is(-0, +0)); // ?  
console.log(Object.is(NaN, 0/0)); // ?