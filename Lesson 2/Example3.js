const obj = {
    name: undefined,
    age: 100,
    foo: () => console.log('hello'),
    rabbit: null,
    tableInTheRoom: true,
    food: 'meat',
    somethingUnique: Symbol(),
}

console.log('Результат преобразования', JSON.stringify(obj)) // ?

